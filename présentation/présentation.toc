\babel@toc {french}{}
\beamer@sectionintoc {1}{Neural melody composition from lyrics}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Mod\`ele}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{Gated Recurrent Unit}{4}{0}{1}
\beamer@subsectionintoc {1}{3}{Encodeur de paroles}{5}{0}{1}
\beamer@subsectionintoc {1}{4}{Encodeur de contexte}{6}{0}{1}
\beamer@subsectionintoc {1}{5}{D\'ecodeur}{8}{0}{1}
\beamer@sectionintoc {2}{Conditional LSTM-GAN for Melody Generation from Lyrics}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Mod\`ele}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{Encodeur de paroles}{10}{0}{2}
\beamer@subsectionintoc {2}{3}{LSTM}{11}{0}{2}
\beamer@subsectionintoc {2}{4}{GAN}{12}{0}{2}
